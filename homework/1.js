var button = document.querySelectorAll('.showButton');
var header = document.getElementById('buttonContainer');
var tab_items = document.querySelectorAll('.tab');
var buttonHide = document.createElement('button');
    buttonHide.setAttribute('onClick', 'hideAllTabs()');
    buttonHide.classList.add('showButton');
    buttonHide.innerText = 'Close tab';
    button.forEach(function (item, i) {
        button[i].onclick = function (event) {
            event.preventDefault();
            tab_items.forEach(function (item, i) {
                // tab_items[i].dataset.tab;
                if(tab_items[i].dataset.tab === event.target.dataset.tab){
                    tab_items[i].classList.add('active');
                    console.log(tab_items[i]);
                        }else {
                            tab_items[i].classList.remove('active');
                        };


            })
            // for(var i = 0; i < tab_items.length; i++) {
            //     tab_items[i];
            //     if(tab_items[i].dataset.tab === event.target.dataset.tab){
            //         tab_items[i].classList.add('active');
            //     }else {
            //         tab_items[i].classList.remove('active');
            //     };
            // };
        }
});
    function hideAllTabs(){
        tab_items.forEach(function (item, i) {
            if(tab_items[i].classList.contains('active')){
                tab_items[i].classList.remove('active');
            }
        })
    }
    header.appendChild(buttonHide);

  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
         event.preventDefault();

      }

  */
